# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: org-eproject
#+subtitle: Manage Org projects with eproject
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle org-eproject.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Meta
** Metadata
#+begin_src elisp :results none
;; Author: Dima Akater <nuclearspace@gmail.com>
;; Maintainer: Dima Akater <nuclearspace@gmail.com>
;; Keywords: org, projects

#+end_src

** Dependencies
#+begin_src elisp :results none
(require 'cl-lib)
(require 'cl-extra)
(require 'cl-generic)
(eval-when-compile (require 'cl-macs))
(require 'cl-seq)
(require 'eieio-base)
;; (eval-when-compile (require 'eieio-akater-extras))
(require 'etemplate-eproject)
(require 'etemplate-create)
#+end_src

#+begin_src elisp :results none
(eval-when-compile
  (setq-local byte-compile-warnings
              (cond
               ((listp byte-compile-warnings)
                (remq 'obsolete byte-compile-warning-types))
               (t '( redefine callargs free-vars unresolved
                     noruntime cl-functions interactive-only make-local mapcar
                     constants suspicious lexical)))
              byte-compile-warning-types
              (cond
               ((listp byte-compile-warning-types)
                (remq 'obsolete byte-compile-warning-types))
               (t '( redefine callargs free-vars unresolved
                     noruntime cl-functions interactive-only make-local mapcar
                     constants suspicious lexical)))))
#+end_src

* Customizable: group org-eproject
** Defgroup
#+begin_src elisp :results none
(defgroup org-eproject nil
  "Generic projects written and managed in Org"
  :tag "Org Project"
  :prefix "org-eproject-"
  :group 'org
  :link '(url-link :tag "GitLab repository"
                   "https://gitlab.com/akater/org-eproject")
  :link `(url-link :tag "Send Bug Report"
                   ,(concat "mailto:" "nuclearspace@gmail.com" "?subject=\
org-eproject bug: \
&body=Describe bug here, starting with `emacs -Q'.  \
Don't forget to mention your Emacs and `org-eproject' versions.")))
#+end_src

** author-string
#+begin_src elisp :results none
(defcustom org-eproject-author-string user-full-name
  "The value of #+author: parameter in created projects"
  :group 'org-eproject
  :type 'string)
#+end_src

** default-persistence-file
#+begin_src elisp :results none
(defcustom org-eproject-default-persistence-file "~/.config/emacs/org-eprojects"
  "File to record all initialized org-eprojects to by default"
  :group 'org-eproject
  :type 'file)
#+end_src

* TODO persistence
  - State "TODO"       from "TODO"       [2020-03-05 Thu 20:38] \\
    ~/.config/emacs/org-eprojects is not created and not filled with anything if created manually

* TODO Org Projects
- State "TODO"       from              [2021-12-13 Mon 23:21]
** projects
#+begin_src elisp :results none
(defvar org-eprojects nil)
#+end_src

#+begin_src elisp :tangle no :results none :eval never
(defclass org-eprojects-db
  ((records :accessor org-eproject-records :type list)))
#+end_src

** Definition
#+begin_src elisp :results none
(defclass org-eproject (etemplate--eproject)
  (
   ;; (tracking-symbol :initform 'org-eprojects)
   )
  :documentation "Superclass for all Org projects.  Not meant to be instantiated for now.")
#+end_src

** DEPRECATED all-project-classes
- State "DEPRECATED" from              [2023-06-30 Fri 22:27]
#+begin_src elisp :tangle no :results none
(defvar org-eproject-all-classes nil)
#+end_src

** DEPRECATED define-project-class
- State "DEPRECATED" from              [2023-06-30 Fri 22:27]
#+begin_src elisp :tangle no :results none
(defmacro define-org-eproject-class (name superclasses slots &rest options-and-doc)
  "Like defclass but always inherits from org-eproject, and records the class in `org-eproject-all-project-classes'.

The class created is captured as CLASS."
  (cl-check-type name (and symbol (not null) (not (eql t)) (not keyword)))
  ;; (cl-check-type superclasses (list-of symbol))
  `(let ((class (defclass ,name (,@superclasses org-eproject)
                  ,slots ,@options-and-doc)))
     (cl-pushnew ',name org-eproject-all-classes :test #'eq)
     class))
#+end_src

** DEPRECATED class-read
- State "DEPRECATED" from              [2023-06-30 Fri 22:27]
#+begin_src elisp :tangle no :results none
(defun org-eproject-class-read ()
  (if (null org-eproject-all-classes) (error "No project classes defined")
    (completing-read "Project (class): "
                     org-eproject-all-classes nil t)))
#+end_src

* Misc
** org-eproject-files
*** Prerequisites
**** readme-file-p
***** Definition
#+begin_src elisp :tangle no :results none
(defun readme-file-p (filename)
  (string-equal "README" (file-name-base filename)))
#+end_src

**** flatten
***** Definition
#+begin_src elisp :tangle no :results none
(defsubst flatten(x)
    (cond ((null x) nil)
	  ((listp x) (append (flatten (car x)) (flatten (cdr x))))
	  (t (list x))))
#+end_src

*** Definition
#+begin_src elisp :tangle no :results none
(defun org-eproject-files ()
  "Returns all org files in all projects sans README.org, intended for use in Org Agenda."
  (flatten
   (mapcar (lambda (org-eproject)
             (cl-remove-if #'readme-file-p
                        (find-lisp-find-files (eproject-directory org-eproject)
                                              "\\.org$")))
           org-eprojects)))
#+end_src

* (experimental) native classes
** native-classes
#+begin_src elisp :tangle no :results none :eval never
(defgeneric/mc org-eproject-native-classes (template)
  :method-combination append)
#+end_src

** read-native-class-name
#+begin_src elisp :tangle no :results none :eval never
(defun org-eproject-read-native-class-name (template)
  (completing-read "Project class: "
                   (org-eproject-native-classes template)))
#+end_src

* Methods
** Methods: org-eproject
#+begin_src elisp :results none
(cl-defmethod etemplate-initialize :replace-string ((_template etemplate)
                                                    (_org-eproject
                                                     org-eproject))
  "Substitute author name"
  (list "%org-author-string%" org-eproject-author-string))
#+end_src

** TODO print-object org-eproject

* TODO (move to org-development) org-agenda-custom-commands
This should actually be done in org-development as it mentions TEST-PASSED, etc. However, at least some interface to record various kinds of projects in agenda should be provided by org-eproject.
#+begin_src elisp :tangle no :results none :eval never
(setq org-agenda-custom-commands
      (append org-agenda-custom-commands
          `(("p" . "Search in org-eprojects")
           ("pt" "All tests in all projects" todo "TEST-FAILED|TEST-PASSED"
	    ((org-agenda-files (org-eproject-files))))
           ("pd" "Project notes deprecated/obsolete search" todo "DEPRECATED|OBSOLETE"
	    ((org-agenda-files (org-eproject-files))))
           ("ps" "Project notes full-text search" search ""
	    ((org-agenda-files (org-eproject-files)))))
	  ))
#+end_src

* TODO Maybe define class for Org templates
- State "TODO"       from              [2021-12-13 Mon 23:20]
