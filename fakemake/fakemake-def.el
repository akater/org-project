;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'etemplate
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '( "etemplate-core"
                        "etemplate-eproject"
                        "etemplate-create"
                        "etemplate"
                        "org-eproject")
  site-lisp-config-prefix "50"
  license "GPL-3")
